import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'TestMQTT';
  private subscription: Subscription;
  topicname: any;
  msg: any;
  isConnected: boolean = false;

  constructor(private _mqttService: MqttService) { }

  ngOnInit(): void {
    console.log('inside subscribe new topic')
    
    this.topicname = '/VehicleTracking/#';
    this.subscription = this._mqttService.observe(this.topicname).subscribe((message: IMqttMessage) => {
      this.msg = message;
      console.log('msg: ', message.payload.toString() )
    
    });
  
    console.log(this.subscription);

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscribeNewTopic(): void {

  }
}
